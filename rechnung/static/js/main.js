$(document).ready(function() {

    // Pagination
    function pagination() {
        rechnungsempfaenger = $("#rechnungsempfaenger");
        $(".pages").on("click", function(e){
            var href = $(this).attr("href");
            rechnungsempfaenger.fadeTo("fast", 0.1, function() {
                rechnungsempfaenger.load(href + " #rechnungsempfaenger > *", function () {
                    rechnungsempfaenger.fadeTo("slow", 1);
                    pagination();
                });
            })
            e.preventDefault();
        });
    }
    pagination();

});
