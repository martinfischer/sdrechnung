# -*- coding: utf-8 -*-
from .models import Rechnungsempfaenger
from django.contrib import admin


class RechnungsempfaengerAdmin(admin.ModelAdmin):
    """
    Das Admin Model für Rechnungsempfänger.
    """
    list_display = (
        "firma",
        "name",
        "adresse",
        "plz",
        "ort",
        "land",
        "uid",
    )
    ordering = ["firma"]

admin.site.register(Rechnungsempfaenger, RechnungsempfaengerAdmin)
