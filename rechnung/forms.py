# -*- coding: utf-8 -*-
from .models import Rechnungsempfaenger
from django import forms
from datetime import date
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field
from crispy_forms.bootstrap import AppendedText


class RechnungsempfaengerForm(forms.ModelForm):
    """
    Das Formular für einen neuen Rechnungsempfänger Eintrag.
    """

    firma = forms.CharField(
        error_messages={
            "unique": u"Rechnungsempfänger mit diesem Namen existiert bereits."}
    )

    class Meta:
        model = Rechnungsempfaenger

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.help_text_inline = True
        self.helper.layout = Layout(
            Field("firma", css_class="span4"),
            Field("name", css_class="span4"),
            Field("adresse", css_class="span4"),
            Field("plz", css_class="span4"),
            Field("ort", css_class="span4"),
            Field("land", css_class="span4"),
            Field("uid", css_class="span4"),
        )
        super(RechnungsempfaengerForm, self).__init__(*args, **kwargs)


class RechnungsForm(forms.Form):
    """
    Das Formular für die Rechnung.
    """

    today = date.today()
    formated_date = today.strftime("%d.%m.%Y")

    button_rechnungsempfaenger = """<a href="/rechnungsempfaenger/neu/" class="btn">
    <i class="icon-plus"></i></a>
    """

    rechnungsempfaenger = forms.ModelChoiceField(
        label="Rechnungsempfänger",
        queryset=Rechnungsempfaenger.objects.all(),
        help_text=button_rechnungsempfaenger,
        required=True,
    )

    rechnungs_nummer = forms.CharField(
        label="Rechnungsnummer",
        max_length=50,
        help_text="(Die Rechnungsnummer benennt auch das PDF)",
        required=True,
    )

    rechnungs_titel = forms.CharField(
        label="Rechnungstitel",
        max_length=100,
        initial="Live Auftritt souldavis am {}".format(formated_date),
        required=True,
    )

    rechnungs_gage = forms.DecimalField(
        label="Gage ohne MwSt",
        help_text="(Zulässig sind Werte wie: 50, 50.5 oder 50,55)",
        localize=True,
        max_digits=10,
        decimal_places=2,
        required=True,
    )

    rechnungs_datum = forms.DateField(
        label="Rechnungsdatum",
        help_text="Immer in dieser Form: 22.04.2014",
        localize=True,
        initial="{}".format(formated_date),
        required=True,
    )

    rechnung_bar = forms.BooleanField(
        required=False,
        label="Wird in bar bezahlt",
        initial=True,
    )

    def __init__(self, *args, **kwargs):
        """
        Fügt Feldern CSS und Bootstrap Styling hinzu.
        """
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.help_text_inline = True
        self.helper.layout = Layout(
            Field("rechnungsempfaenger", css_class="span4"),
            Field("rechnungs_datum", css_class="span4"),
            Field("rechnungs_nummer", css_class="span4"),
            Field("rechnungs_titel", css_class="span4"),
            Field(AppendedText("rechnungs_gage", "€", css_class="span4",)),
            Field("rechnung_bar"),
        )
        super(RechnungsForm, self).__init__(*args, **kwargs)
