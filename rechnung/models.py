# -*- coding: utf-8 -*-
from django.db import models


class Rechnungsempfaenger(models.Model):
    """
    Das ORM Model für Rechnungsempfänger.
    """
    firma = models.CharField(max_length=200, blank=False, unique=True)
    name = models.CharField(max_length=200, blank=True)
    adresse = models.CharField(max_length=200, blank=True)
    plz = models.CharField(max_length=20, blank=True)
    ort = models.CharField(max_length=200, blank=True)
    land = models.CharField(max_length=200, blank=True)
    uid = models.CharField(max_length=20, blank=True, verbose_name="UID")

    def __unicode__(self):
        return self.firma

    class Meta:
        verbose_name = "Rechnungsempfänger"
        verbose_name_plural = "Rechnungsempfänger"

