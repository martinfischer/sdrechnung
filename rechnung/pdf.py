# -*- coding: utf-8 -*-
import os
from reportlab.lib.pagesizes import A4, cm
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer
from reportlab.platypus import Table, TableStyle
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.enums import TA_RIGHT, TA_LEFT, TA_CENTER
from reportlab.lib import colors
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont


def make_pdf(data, bar=True, font_folder="rechnung/ubuntu-font-family-0.80/"):
    """
    Erstellt PDF Rechnungen.
    Nimmt ein dict als erstes Argument und optional ein Keyword Argument namens
    "font_folder", womit man angeben kann wo sich der Ordner der "Ubuntu"
    Schriftart befindet.

    Verwendungsbeispiel:
    make_pdf({
        "pdf_fileobject": u"filename.pdf",  # oder Django HttpResponse Objekt
        "pdf_title": u"Honorarnote 0815",
        "pdf_author": u"Martin Fischer",
        "pdf_subject": u"Rechnung erstellt von souldavis Software",
        "pdf_creator": u"souldavis",
        "pdf_keywords": u"souldavis, Martin, Fischer",
        "receiver_address_company": u"Firma",
        "receiver_address_name": u"Eric Idle",
        "receiver_address_street": u"Straße 99",
        "receiver_address_zip_city": u"9999 Ort",
        "receiver_address_country": u"Österreich",
        "receiver_address_uid": u"UID: 987654321",
        "rechnungs_nummer": u"Rechnung SD-0815",
        "rechnungs_titel": u"Live Auftritt souldavis",
        "rechnungs_summe_netto": u"100,00",
        "rechnungs_summe_mwst": u"10,00",
        "rechnungs_summe_brutto": u"110,00",
        "rechnungs_datum": u"22.02.2012"
    }, font_folder="rechnung/ubuntu-font-family-0.80/")
    """

    # Registriert die Schriftart Ubuntu.
    pdfmetrics.registerFont(TTFont("Ubuntu", os.path.join(font_folder,
                                                          "Ubuntu-R.ttf")))
    pdfmetrics.registerFont(TTFont("UbuntuBold", os.path.join(font_folder,
                            "Ubuntu-B.ttf")))
    pdfmetrics.registerFont(TTFont("UbuntuItalic", os.path.join(font_folder,
                            "Ubuntu-RI.ttf")))
    pdfmetrics.registerFontFamily("Ubuntu",
                                  normal="Ubuntu",
                                  bold="UbuntuBold",
                                  italic="UbuntuItalic")

    # Hier werden alles Styles aufgesetzt.
    page_width, page_height = A4
    font_size_p = 12
    font_size_h1 = 16
    color_h1 = colors.HexColor("#556AA6")

    styles = getSampleStyleSheet()

    styles.add(ParagraphStyle(name="p-left",
                              fontName="Ubuntu",
                              fontSize=font_size_p,
                              leading=font_size_p + 2,
                              leftIndent=0,
                              rightIndent=0,
                              firstLineIndent=0,
                              alignment=TA_LEFT,
                              spaceBefore=0,
                              spaceAfter=0,
                              bulletFontName="Ubuntu",
                              bulletFontSize=font_size_p,
                              bulletIndent=0,
                              textColor=colors.black,
                              backColor=None,
                              wordWrap=None,
                              borderWidth=0,
                              borderPadding=0,
                              borderColor=None,
                              borderRadius=None,
                              allowWidows=1,
                              allowOrphans=0))

    styles.add(ParagraphStyle(name="p-right",
                              fontName="Ubuntu",
                              fontSize=font_size_p,
                              leading=font_size_p + 2,
                              leftIndent=0,
                              rightIndent=0,
                              firstLineIndent=0,
                              alignment=TA_RIGHT,
                              spaceBefore=0,
                              spaceAfter=0,
                              bulletFontName="Ubuntu",
                              bulletFontSize=font_size_p,
                              bulletIndent=0,
                              textColor=colors.black,
                              backColor=None,
                              wordWrap=None,
                              borderWidth=0,
                              borderPadding=0,
                              borderColor=None,
                              borderRadius=None,
                              allowWidows=1,
                              allowOrphans=0))

    styles.add(ParagraphStyle(name="h1_left",
                              fontName="Ubuntu",
                              fontSize=font_size_h1,
                              leading=font_size_h1 + 2,
                              leftIndent=0,
                              rightIndent=0,
                              firstLineIndent=0,
                              alignment=TA_LEFT,
                              spaceBefore=0,
                              spaceAfter=0,
                              bulletFontName="Ubuntu",
                              bulletFontSize=font_size_h1,
                              bulletIndent=0,
                              textColor=color_h1,
                              backColor=None,
                              wordWrap=None,
                              borderWidth=0,
                              borderPadding=0,
                              borderColor=None,
                              borderRadius=None,
                              allowWidows=1,
                              allowOrphans=0))

    styles.add(ParagraphStyle(name="table-center",
                              fontName="Ubuntu",
                              fontSize=font_size_p,
                              leading=font_size_p + 2,
                              leftIndent=0,
                              rightIndent=0,
                              firstLineIndent=0,
                              alignment=TA_CENTER,
                              spaceBefore=0,
                              spaceAfter=0,
                              bulletFontName="Ubuntu",
                              bulletFontSize=font_size_p,
                              bulletIndent=0,
                              textColor=colors.black,
                              backColor=None,
                              wordWrap=None,
                              borderWidth=0,
                              borderPadding=0,
                              borderColor=None,
                              borderRadius=None,
                              allowWidows=1,
                              allowOrphans=0))

    styles.add(ParagraphStyle(name="table-left",
                              fontName="Ubuntu",
                              fontSize=font_size_p,
                              leading=font_size_p + 2,
                              leftIndent=0,
                              rightIndent=0,
                              firstLineIndent=0,
                              alignment=TA_LEFT,
                              spaceBefore=0,
                              spaceAfter=0,
                              bulletFontName="Ubuntu",
                              bulletFontSize=font_size_p,
                              bulletIndent=0,
                              textColor=colors.black,
                              backColor=None,
                              wordWrap=None,
                              borderWidth=0,
                              borderPadding=0,
                              borderColor=None,
                              borderRadius=None,
                              allowWidows=1,
                              allowOrphans=0))

    styles.add(ParagraphStyle(name="table-right",
                              fontName="Ubuntu",
                              fontSize=font_size_p,
                              leading=font_size_p + 2,
                              leftIndent=0,
                              rightIndent=0,
                              firstLineIndent=0,
                              alignment=TA_RIGHT,
                              spaceBefore=0,
                              spaceAfter=0,
                              bulletFontName="Ubuntu",
                              bulletFontSize=font_size_p,
                              bulletIndent=0,
                              textColor=colors.black,
                              backColor=None,
                              wordWrap=None,
                              borderWidth=0,
                              borderPadding=0,
                              borderColor=None,
                              borderRadius=None,
                              allowWidows=1,
                              allowOrphans=0))

    styles.add(ParagraphStyle(name="table-left-small",
                              fontName="Ubuntu",
                              fontSize=font_size_p - 2,
                              leading=font_size_p,
                              leftIndent=0,
                              rightIndent=0,
                              firstLineIndent=0,
                              alignment=TA_LEFT,
                              spaceBefore=0,
                              spaceAfter=0,
                              bulletFontName="Ubuntu",
                              bulletFontSize=font_size_p - 2,
                              bulletIndent=0,
                              textColor=colors.black,
                              backColor=None,
                              wordWrap=None,
                              borderWidth=0,
                              borderPadding=0,
                              borderColor=None,
                              borderRadius=None,
                              allowWidows=1,
                              allowOrphans=0))

    # So sieht die erste Seite aus.
    def first_page(canvas, doc):
        canvas.saveState()
        canvas.setFont("Ubuntu", 8)
        canvas.setFillColor(colors.gray)
        canvas.drawCentredString(page_width / 2.0, 20,
                                 "Seite {}".format(doc.page))
        canvas.restoreState()

    # So sehen alle weiter Seiten aus. Identisch zur ersten Seite.
    def further_pages(canvas, doc):
        canvas.saveState()
        canvas.setFont("Ubuntu", 8)
        canvas.setFillColor(colors.gray)
        canvas.drawCentredString(page_width / 2.0, 20,
                                 "Seite {}".format(doc.page))
        canvas.restoreState()

    # Die Story beginnt hier.
    story = []

    story.append(Spacer(1, font_size_p))

    # Die Daten des Rechnungssenders.
    sender_address_parts = [u"souldavis",
                            u"Martin Fischer",
                            u"Innsbruckerstraße 6a",
                            u"6380 St. Johann in Tirol",
                            u"Österreich",
                            u"UID: ATU64650602"]

    for part in sender_address_parts:
        ptext = u"<font>{}</font>".format(part)
        story.append(Paragraph(ptext, styles["p-left"]))

    story.append(Spacer(1, font_size_p * 4))

    # Die Daten des Rechnungsempfängers.
    receiver_address_parts = [data["receiver_address_company"],
                              data["receiver_address_name"],
                              data["receiver_address_street"],
                              data["receiver_address_zip_city"],
                              data["receiver_address_country"],
                              data["receiver_address_uid"]]

    for part in receiver_address_parts:
        ptext = u"<font>{}</font>".format(part)
        story.append(Paragraph(ptext, styles["p-left"]))

    story.append(Spacer(1, font_size_p))

    # Der Ort und das Datum.
    sender_address_city = "St. Johann in Tirol"
    rechnungs_datum = data["rechnungs_datum"]
    formated_date = rechnungs_datum.strftime("%d.%m.%Y")
    ptext = u"<font>{}, am {}</font>".format(sender_address_city, formated_date)
    story.append(Paragraph(ptext, styles["p-right"]))

    story.append(Spacer(1, font_size_p * 4))

    # Die Rechnungsnummer.
    ptext = u"""<font><b>Honorarnote {}</b></font>
            """.format(data["rechnungs_nummer"])
    story.append(Paragraph(ptext, styles["h1_left"]))

    story.append(Spacer(1, font_size_p * 4))

    # Die Tabelle mit den Rechnungsdaten.
    # Überschriften.
    table_header_position = Paragraph(u"<b>Pos</b>", styles["table-center"])
    table_header_bezeichnung = Paragraph(u"<b>Bezeichnung</b>",
                                         styles["table-center"])
    table_header_summe = Paragraph(u"<b>Summe</b>", styles["table-center"])

    # Reihe 1 - Titel und Summe Netto.
    table_row1_position = Paragraph(u"1", styles["table-center"])
    table_row1_bezeichnung = Paragraph(u"{}".format(data["rechnungs_titel"]),
                                       styles["table-center"])
    table_row1_summe = Paragraph(u"€ {}".format(data["rechnungs_summe_netto"]),
                                 styles["table-right"])

    # Reihe 3 - Summe MWST.
    table_row3_position = Paragraph("", styles["table-center"])
    table_row3_bezeichnung = Paragraph(u"+ 10% MwSt", styles["table-right"])
    table_row3_summe = Paragraph(u"€ {}".format(data["rechnungs_summe_mwst"]),
                                 styles["table-right"])

    # Reihe 10 - Ein paar Striche zur Abgrenzung.
    table_row10_position = Paragraph("", styles["table-center"])
    table_row10_bezeichnung = Paragraph(u"-" * 23, styles["table-right"])
    table_row10_summe = Paragraph("", styles["table-right"])

    # Reihe 11 - Gesamtbetrag und Summe Brutto.
    table_row11_position = Paragraph("", styles["table-center"])
    table_row11_bezeichnung = Paragraph("<b>Gesamtbetrag</b>",
                                        styles["table-right"])
    table_row11_summe = Paragraph(u"<b>€ {}</b>".format(
                                  data["rechnungs_summe_brutto"]),
                                  styles["table-right"])

    # Die Tabelle als Liste aus Listen. Leer bedeutet eine leere Reihe.

    data_rechnung = [
        [table_header_position, table_header_bezeichnung,
         table_header_summe],
        [table_row1_position, table_row1_bezeichnung, table_row1_summe],
        [],
        [table_row3_position, table_row3_bezeichnung, table_row3_summe],
        [],
        [table_row10_position, table_row10_bezeichnung, table_row10_summe],
        [table_row11_position, table_row11_bezeichnung, table_row11_summe],
    ]

    # Styles für die Tabelle.
    table_rechnung = Table(data_rechnung,
                           colWidths=[1.5 * cm, 11 * cm, 3 * cm])

    table_rechnung.setStyle(TableStyle([
                            ("BACKGROUND", (0, 0), (-1, 0), colors.lightblue),
                            ("FONTNAME", (0, 0), (-1, -1), "Ubuntu"),
                            ("FONTSIZE", (0, 0), (-1, -1), 12),
                            ("INNERGRID", (0, 0), (-1, -1), 0.25,
                             colors.black),
                            ("BOX", (0, 0), (-1, -1), 0.25, colors.black),
                            ("VALIGN", (0, 0), (-1, -1), "MIDDLE")]))

    # Die Story wird um eine Tabelle reicher.
    story.append(table_rechnung)

    story.append(Spacer(1, font_size_p * 10))

    if bar:
        # Die Bankinformationen.
        ptext = u"<font>Betrag dankend erhalten: _______________________</font>"
        story.append(Paragraph(ptext, styles["p-left"]))

    else:
        # Die Bankinformationen.
        ptext = u"<font>Den Gesamtbetrag bitte überweisen an:</font>"
        story.append(Paragraph(ptext, styles["p-left"]))

        story.append(Spacer(1, font_size_p))

        sender_bank_parts = [u"Empfänger: Martin Fischer",
                             u"Bank: Sparkasse Kitzbühel",
                             u"IBAN: AT022050500101117166",
                             u"BIC: SPKIAT2KXXX"]

        for part in sender_bank_parts:
            ptext = u"<font>{}</font>".format(part)
            story.append(Paragraph(ptext, styles["p-left"]))


    # Das Template für das Dokument wird aufgesetzt.
    doc = SimpleDocTemplate(data["pdf_fileobject"],
                            pagesize=A4,
                            title=u"{}".format(data["pdf_title"]),
                            author=u"{}".format(data["pdf_author"]),
                            subject=u"{}".format(data["pdf_subject"]),
                            creator=u"{}".format(data["pdf_creator"]),
                            keywords=u"{}".format(data["pdf_keywords"]),
                            rightMargin=70,
                            leftMargin=70,
                            topMargin=20,
                            bottomMargin=20)

    # Das PDF wird erstellt.
    doc.build(story, onFirstPage=first_page, onLaterPages=further_pages)
