# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from .forms import RechnungsForm, RechnungsempfaengerForm
from .models import Rechnungsempfaenger
from .utils import moneyformat
from .pdf import make_pdf
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from decimal import Decimal
from urllib import quote


def index(request):
    """
    Der View für die Index Seite.
    """
    if request.method == "POST":
        form = RechnungsForm(request.POST)

        # Formular nicht valid
        if not form.is_valid():
            return render_to_response("rechnung/index.html",
                                      {"form": form},
                                      context_instance=RequestContext(request))
        if form.is_valid():
            # Die Daten aus dem POST Request
            receiver_address_company = form.cleaned_data["rechnungsempfaenger"].firma
            receiver_address_name = form.cleaned_data["rechnungsempfaenger"].name
            receiver_address_street = form.cleaned_data["rechnungsempfaenger"].adresse
            receiver_address_zip = form.cleaned_data["rechnungsempfaenger"].plz
            receiver_address_city = form.cleaned_data["rechnungsempfaenger"].ort
            receiver_address_zip_city = receiver_address_zip + " " + \
                receiver_address_city
            receiver_address_country = form.cleaned_data["rechnungsempfaenger"].land
            receiver_address_uid = form.cleaned_data["rechnungsempfaenger"].uid
            rechnungs_nummer = form.cleaned_data["rechnungs_nummer"]
            rechnungs_titel = form.cleaned_data["rechnungs_titel"]
            rechnungs_summe = form.cleaned_data["rechnungs_gage"]
            rechnungs_datum = form.cleaned_data["rechnungs_datum"]
            rechnung_bar = form.cleaned_data["rechnung_bar"]


            # Die Rechnungssummen werden ausgerechnet.
            rechnungs_summe_netto = rechnungs_summe
            rechnungs_summe_mwst = rechnungs_summe_netto / Decimal(100) \
                * Decimal(13)
            rechnungs_summe_brutto = rechnungs_summe_netto +\
                rechnungs_summe_mwst

            rechnungs_summe_netto_formated = moneyformat(rechnungs_summe_netto)
            rechnungs_summe_mwst_formated = moneyformat(rechnungs_summe_mwst)
            rechnungs_summe_brutto_formated = moneyformat(
                rechnungs_summe_brutto)

            #Das file Objekt zur PDF erstellung wird erstellt.
            response = HttpResponse(mimetype="application/pdf")
            filename = rechnungs_nummer.encode("utf-8")
            filename = quote(filename)
            url = u"attachment; filename=\"Honorarnote {}.pdf\"".format(filename)
            response["Content-Disposition"] = url

            # Die Daten fürs PDF.
            data = {
                "pdf_fileobject": response,
                "pdf_title": u"Honorarnote {}".format(rechnungs_nummer),
                "pdf_author": u"Martin Fischer",
                "pdf_subject": u"Honorarnote erstellt von webpystunden Software",
                "pdf_creator": u"webpystunden",
                "pdf_keywords": u"webpystunden, Martin, Fischer",
                "receiver_address_company": receiver_address_company,
                "receiver_address_name": receiver_address_name,
                "receiver_address_street": receiver_address_street,
                "receiver_address_zip_city": receiver_address_zip_city,
                "receiver_address_country": receiver_address_country,
                "receiver_address_uid": receiver_address_uid,
                "rechnungs_nummer": rechnungs_nummer,
                "rechnungs_titel": rechnungs_titel,
                "rechnungs_summe_netto": rechnungs_summe_netto_formated,
                "rechnungs_summe_mwst": rechnungs_summe_mwst_formated,
                "rechnungs_summe_brutto": rechnungs_summe_brutto_formated,
                "rechnungs_datum": rechnungs_datum,
            }

            # Das PDF wird erstellt.
            make_pdf(data, bar=rechnung_bar)

            # Das PDF wird an den Browser zum Herunterladen geschickt.
            return response
    else:
        form = RechnungsForm()
    return render_to_response("rechnung/index.html",
                              {"form": form},
                              context_instance=RequestContext(request))


def rechnungsempfaenger(request):
    """
    Der View, um eine Rechnungsempfänger auszuwählen zum Bearbeiten oder zum Löschen.
    Login ist notwendig.
    """
    rechnungsempfaenger_list = Rechnungsempfaenger.objects.all().order_by("firma")
    paginator = Paginator(rechnungsempfaenger_list, 10)
    page = request.GET.get("page")
    try:
        rechnungsempfaenger = paginator.page(page)
    except PageNotAnInteger:
        rechnungsempfaenger = paginator.page(1)
    except EmptyPage:
        rechnungsempfaenger = paginator.page(paginator.num_pages)

    return render_to_response("rechnung/rechnungsempfaenger.html",
                              {"rechnungsempfaenger": rechnungsempfaenger},
                              context_instance=RequestContext(request))


def rechnungsempfaenger_neu(request):
    """
    Der View für einen neuen Firma Eintrag.
    Leitet auf "/" um.
    Login ist notwendig.
    """
    # Falls POST
    if request.method == "POST":
        form = RechnungsempfaengerForm(request.POST)
        if form.is_valid():
            rechnungsempfaenger = RechnungsempfaengerForm(request.POST)
            neuer_eintrag = rechnungsempfaenger.save()
            if neuer_eintrag:
                return HttpResponseRedirect(reverse("index"))

    # Wenn nicht POST dann ein leeres Formular.
    else:
        form = RechnungsempfaengerForm()

    return render_to_response("rechnung/rechnungsempfaenger_neu.html",
                              {"form": form},
                              context_instance=RequestContext(request))


def rechnungsempfaenger_bearbeiten(request, rechnungsempfaenger_id):
    """
    Der View, um einen Rechnungsempfänger zu bearbeiten, oder zu löschen.
    """
    # Request ist POST
    if request.method == "POST":
        # Bearbeiten
        if "rechnungsempfaenger_bearbeiten" in request.POST:
            rechnungsempfaenger = RechnungsempfaengerForm(
                request.POST,
                instance=Rechnungsempfaenger.objects.get(pk=rechnungsempfaenger_id))
            rechnungsempfaenger.save()
            return HttpResponseRedirect(reverse("rechnungsempfaenger"))
        # Löschen
        elif "rechnungsempfaenger_loeschen" in request.POST:
            rechnungsempfaenger = Rechnungsempfaenger.objects.get(pk=rechnungsempfaenger_id)
            rechnungsempfaenger.delete()
            return HttpResponseRedirect(reverse("rechnungsempfaenger"))
        else:
            return HttpResponseRedirect(reverse("rechnungsempfaenger"))

    # Request ist nicht POST
    else:
        rechnungsempfaenger = get_object_or_404(Rechnungsempfaenger, pk=rechnungsempfaenger_id)
        form = RechnungsempfaengerForm(instance=rechnungsempfaenger)
        return render_to_response(
            "rechnung/rechnungsempfaenger_bearbeiten.html",
            {"form": form,
             "rechnungsempfaenger_id": rechnungsempfaenger_id},
            context_instance=RequestContext(request))
