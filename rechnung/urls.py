from django.conf.urls import patterns, url

urlpatterns = patterns(
    'rechnung.views',
    # Home
    url(r'^$', "index", name="index"),

    url(r'^rechnungsempfaenger/$', "rechnungsempfaenger", name="rechnungsempfaenger"),
    url(r'^rechnungsempfaenger/neu/$', "rechnungsempfaenger_neu", name="rechnungsempfaenger_neu"),
    url(r'^rechnungsempfaenger/(?P<rechnungsempfaenger_id>\d+)/$', "rechnungsempfaenger_bearbeiten",
        name="rechnungsempfaenger_bearbeiten"),
)
